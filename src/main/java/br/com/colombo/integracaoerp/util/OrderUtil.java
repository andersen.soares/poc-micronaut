package br.com.colombo.integracaoerp.util;

import br.com.colombo.integracaoerp.model.Item;
import br.com.colombo.integracaoerp.model.Order;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Configuration;
import org.apache.camel.spi.annotations.Component;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Singleton
public class OrderUtil {

    public List<Item> getRandomItens(String orderId) {
        List<Item> itens = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            itens.add(newRandomItem(orderId));
        }
        return itens;
    }

    public Order createRandomOrderWith5Itens() {
        double amount = ThreadLocalRandom.current().nextDouble(1000.00);
        Order order = new Order();
        order.setId(UUID.randomUUID().toString());
        return order;
    }

    public Item newRandomItem(String orderId) {
        Item item = new Item();
        item.setId(UUID.randomUUID().toString());
        String randomData = getRandomString();
        item.setOne(randomData);
        item.setOrderid(orderId);
        item.setTwo(randomData);
        item.setTree(randomData);
        item.setFour(randomData);
        item.setFive(randomData);
        item.setSix(randomData);
        item.setSeven(randomData);
        item.setEight(randomData);
        item.setNine(randomData);
        item.setTen(randomData);
        return item;
    }

    private String getRandomString() {
        Random random = new Random();
        return String.valueOf(random.nextLong());
    }

}
