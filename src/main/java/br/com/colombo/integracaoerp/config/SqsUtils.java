package br.com.colombo.integracaoerp.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.JndiRegistry;

import java.util.Random;


public class SqsUtils {

    private static final String QUEUE = "test_consumer_queue_message_dead_letter";
//    private static final String QUEUE = "consumer_queue_message_dead_letter";
    private static final String BIND_SQS_CLIENT = "sqsClient";

    public static AmazonSQS getSqsClient() {
        BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials("AKIAJEDAAIVKZJRI7ASQ", "emLytu9yWu3e3ZQlNR4m59S5IVD3ipHgHUVpR4vv");
        return AmazonSQSClient
                .builder()
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
                .withRegion(Regions.US_EAST_1)
                .build();
    }

    public static void testSendMessage() throws Exception {
        AmazonSQS sqs = SqsUtils.getSqsClient();
        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(QUEUE)
                .withDelaySeconds(1)
                .withMessageBody("hello consumer_queue_message_dead_letter" + new Random().nextInt());
        sqs.sendMessage(send_msg_request);
    }

    public static void configListenerQueueTest() throws Exception {
        JndiRegistry registry = new JndiRegistry();
        registry.bind(BIND_SQS_CLIENT, SqsUtils.getSqsClient());
        DefaultCamelContext camelContext = new DefaultCamelContext(registry);
        camelContext.addRoutes(getRouterSqs());
        camelContext.start();
    }

    private static RouteBuilder getRouterSqs() {
        return new RouteBuilder() {
            @Override
            public void configure() {
                Processor process = exchange -> {
                    Object body = exchange.getIn().getBody();
                    System.out.println(body);
                };
                from("aws-sqs://" + QUEUE + "?amazonSQSClient=#" + BIND_SQS_CLIENT + "&deleteIfFiltered=false")
                        .process(process);
            }
        };
    }

}
