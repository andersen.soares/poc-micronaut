package br.com.colombo.integracaoerp.repository;

import br.com.colombo.integracaoerp.model.Order;

public interface OracleXERepository {

    Order save(Order order);
}
