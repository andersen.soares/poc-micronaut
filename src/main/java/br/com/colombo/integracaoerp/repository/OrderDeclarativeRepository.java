package br.com.colombo.integracaoerp.repository;

import br.com.colombo.integracaoerp.model.Order;
import io.micronaut.core.annotation.TypeHint;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;
import io.micronaut.http.client.annotation.Client;

@JdbcRepository(dialect = Dialect.ORACLE)
@TypeHint(typeNames = {"com.oracle.ojdbc:ojdbc8:19.3.0.0"}, accessType =  TypeHint.AccessType.ALL_PUBLIC)
public interface OrderDeclarativeRepository extends CrudRepository<Order, Long> {

    @Query("INSERT INTO ORDERS(id, one) " +
            "   VALUES (:id, :one)")
    int insertBean(Order user);

}
