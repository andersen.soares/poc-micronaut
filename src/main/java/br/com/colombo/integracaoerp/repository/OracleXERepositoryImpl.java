package br.com.colombo.integracaoerp.repository;

import br.com.colombo.integracaoerp.model.Order;
import br.com.colombo.integracaoerp.resource.GreetingsControllerRoutes;
import io.micronaut.core.annotation.TypeHint;
import io.micronaut.runtime.ApplicationConfiguration;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@TypeHint(typeNames = {"com.oracle.ojdbc:ojdbc8:19.3.0.0"}, accessType =  TypeHint.AccessType.ALL_PUBLIC)
@Singleton
public class OracleXERepositoryImpl implements OracleXERepository {

    @PersistenceContext
    private EntityManager entityManager;

    private final ApplicationConfiguration applicationConfiguration;

    public OracleXERepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Transactional
    public Order save(Order order){
        entityManager.persist(order);
        return order;
    }
}
