package br.com.colombo.integracaoerp.repository;

import br.com.colombo.integracaoerp.model.Item;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;

@JdbcRepository(dialect = Dialect.ORACLE)
public interface ItemDeclarativeRepository extends CrudRepository<Item, Long> {

    @Query("INSERT INTO items(id, orderid, one, two, tree, four, five, six, seven, eight, nine, ten) " +
            "   VALUES (:id, :orderid, :one, :two, :tree, :four, :five, :six, :seven, :eight, :nine, :ten)")
    int insertBean(Item user);

}
