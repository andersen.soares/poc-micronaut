package br.com.colombo.integracaoerp;

import br.com.colombo.integracaoerp.model.Order;
import br.com.colombo.integracaoerp.repository.OrderDeclarativeRepository;
import io.micronaut.context.event.StartupEvent;
import io.micronaut.core.annotation.TypeHint;
import io.micronaut.runtime.Micronaut;
import io.micronaut.runtime.event.annotation.EventListener;

import javax.inject.Singleton;

@TypeHint(typeNames = {"com.oracle.ojdbc:ojdbc8:19.3.0.0"}, accessType =  TypeHint.AccessType.ALL_PUBLIC)
@Singleton
public class Application {

    private final OrderDeclarativeRepository orderDeclarativeRepository;

    public Application(OrderDeclarativeRepository orderDeclarativeRepository) {
        this.orderDeclarativeRepository = orderDeclarativeRepository;
    }

    public static void main(String[] args) throws Exception {
        Micronaut.run(Application.class);
    }

    @EventListener
    void init(StartupEvent event) throws Exception {

        Order order = new Order();
        order.setId("id");
        order.setOne("one");
        orderDeclarativeRepository.save(order);

        System.out.println("# START # JDBC - INSERT BATCH WITH NUMBER ORDERS= ");

    }
}