package br.com.colombo.integracaoerp.resource;

import br.com.colombo.integracaoerp.config.SqsUtils;
import br.com.colombo.integracaoerp.model.Order;
import br.com.colombo.integracaoerp.repository.ItemDeclarativeRepository;
import br.com.colombo.integracaoerp.repository.OrderDeclarativeRepository;
import br.com.colombo.integracaoerp.util.OrderUtil;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.validation.Validated;

@Validated
@Controller("/greetings")
public class GreetingsController implements GreetingsControllerRoutes {

    private final OrderUtil orderUtil;

    private final OrderDeclarativeRepository orderDeclarativeRepository;

    //protected final OracleXERepository oracleXERepository;

    private static final int NUMBERS_ORDERS = 10;

    public GreetingsController(OrderUtil orderUtil, OrderDeclarativeRepository orderDeclarativeRepository) {
        this.orderUtil = orderUtil;
        this.orderDeclarativeRepository = orderDeclarativeRepository;
    }

    @Get("/")
    public HttpStatus index() throws Exception {
        return HttpStatus.OK;
    }

    @Get("/send-message")
    public HttpStatus sendMssage() throws Exception {
        SqsUtils.testSendMessage();
        return HttpStatus.OK;
    }

    @Get("/init-listener")
    public HttpStatus initListener() throws Exception {
        SqsUtils.configListenerQueueTest();
        return HttpStatus.OK;
    }

    @Override
    public HttpResponse<String> greeting(String name) {
        return HttpResponse.ok(name + "!");
    }

    @Get("/insert-all-one-by-one")
    public void insertBatchAllOneByOne() {
        System.out.println("# START # JDBC - INSERT BATCH WITH NUMBER ORDERS= " + NUMBERS_ORDERS);

        Order order = new Order();
        order.setId("id");
        order.setOne("one");
        orderDeclarativeRepository.insertBean(order);

        System.out.println("# FINISH # JDBC - INSERT BATCH WITH NUMBER ORDERS= " + NUMBERS_ORDERS);

       /* for (int i = 0; i < NUMBERS_ORDERS; i++) {
            Order order1 = orderUtil.createRandomOrderWith5Itens();
            orderDeclarativeRepository.insertBean(order1);
            order.getItens().forEach(it -> itemDeclarativeRepository.insertBean(it));
        }*/
    }

}