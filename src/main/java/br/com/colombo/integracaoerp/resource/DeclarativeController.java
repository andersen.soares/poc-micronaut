package br.com.colombo.integracaoerp.resource;

import br.com.colombo.integracaoerp.model.Order;
import br.com.colombo.integracaoerp.repository.ItemDeclarativeRepository;
import br.com.colombo.integracaoerp.repository.OrderDeclarativeRepository;
import br.com.colombo.integracaoerp.util.OrderUtil;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import javax.inject.Inject;

@Controller("/declarative")
public class DeclarativeController {

    private final OrderUtil orderUtil;

    private final OrderDeclarativeRepository orderDeclarativeRepository;

    private final ItemDeclarativeRepository itemDeclarativeRepository;

    private static final int NUMBERS_ORDERS = 10;

    @Inject
    public DeclarativeController(OrderUtil orderUtil, OrderDeclarativeRepository orderDeclarativeRepository, ItemDeclarativeRepository itemDeclarativeRepository) {
        this.orderUtil = orderUtil;
        this.orderDeclarativeRepository = orderDeclarativeRepository;
        this.itemDeclarativeRepository = itemDeclarativeRepository;
    }

    @Get("/insert-all-one-by-one")
    public void insertBatchAllOneByOne() {
        System.out.println("# JDBC - INSERT BATCH WITH NUMBER ORDERS= " + NUMBERS_ORDERS);
        for (int i = 0; i < NUMBERS_ORDERS; i++) {
            //Order order = orderUtil.createRandomOrderWith5Itens();
            //orderDeclarativeRepository.insertBean(order);
            //order.getItens().forEach(it -> itemDeclarativeRepository.insertBean(it));
        }
    }


}