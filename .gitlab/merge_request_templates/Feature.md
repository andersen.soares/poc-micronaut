## Objetivo
- (descrever brevemente os objetivos atingidos através do MR)

### Task
- (link do TFS)

### Dependência (quando houver)
- (nome do projeto): (link do MR)

### Prioridade (quando houver)
- (descrever brevemente motivo para priorizar aprovação deste MR)

## Checklist Desenvolvimento

### Antes de iniciar o desenvolvimento
* [ ] Li a descrição da Task e a US relacionada
* [ ] Estou seguro do meu entendimento
       <sup>Caso exista duvidas validar com Analista.</sup>
* [ ] Estou seguro da solução que vou aplicar
       <sup>Caso exista duvidas validar com LT.</sup>
* [ ] Não tenho outros impedimentos

### Antes de liberar o MR
* [ ] Eu atendi todas as funcionalidades da tarefa
* [ ] Eu testei a atividade manualmente
       <sup>Simular a interação do usuário.</sup>
* [ ] Eu criei testes unitários
* [ ] Eu revisei meu próprio código

